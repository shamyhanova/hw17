package kz.kaspi.Lesson18.models;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "books")
public class Book implements Serializable {
     @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "author")
    private String author;

    @Column(name = "izdanie")
    private String izdanie;
    @Column(name = "releaseDate")
    private Integer releaseDate;


    public Book() {
    }

    public Book(  String name, String author, String izdanie, Integer releaseDate) {
        this.name = name;
        this.author = author;
        this.izdanie = izdanie;
        this.releaseDate = releaseDate;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIzdanie() {
        return izdanie;
    }

    public void setIzdanie(String izdanie) {
        this.izdanie = izdanie;
    }

    public Integer getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Integer releaseDate) {
        this.releaseDate = releaseDate;
    }
}
