package kz.kaspi.Lesson18.util;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static HibernateUtil instance;
    private static SessionFactory sessionFactory;

    private HibernateUtil() {
        try {
            sessionFactory = new Configuration()
                    .configure()
                    .buildSessionFactory();
        }
        catch (HibernateException e) {
            e.printStackTrace();
            System.out.println("Could not create an instance of HibernateUtil");
        }
    }

    public static HibernateUtil getInstance() {
        if (instance == null)
            instance = new HibernateUtil();
        return instance;
    }

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static SessionFactory obtainSessionFactory() {
        return getInstance().getSessionFactory();
    }

    public static void closeSessionFactory() {
        getInstance().getSessionFactory().close();
    }
}
