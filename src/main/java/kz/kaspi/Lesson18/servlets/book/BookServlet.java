package kz.kaspi.Lesson18.servlets.book;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import kz.kaspi.Lesson18.models.Book;
import kz.kaspi.Lesson18.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "EmployeeServlet", value = {"/book/save"})
public class BookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // список записей из таблицы
        // jsp страница с таблицей
        // передать в jsp страницу список записей

        getList(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String author = request.getParameter("author");
        String izdanie = request.getParameter("izdanie");
        Integer releaseDate = Integer.valueOf(request.getParameter("releaseDate"));

        System.out.println("---POST name = " + name + ", author = " + author + ", izdanie = " + izdanie);

        Session session = HibernateUtil.obtainSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Long id = (Long) session.save(new Book(
                name,
                author,
                izdanie,
                releaseDate
        ));

        transaction.commit();
        session.close();

        System.out.println("Сохранен сотрудник ID = " + id);

        getList(request, response);
    }


    private void getList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Session session = HibernateUtil.obtainSessionFactory().openSession();
        List<Book> resultList = session
                .createQuery("from Book ", Book.class)
                .getResultList();

        session.close();

        request.setAttribute("book", resultList);

        getServletContext().getRequestDispatcher("/book/list.jsp").forward(request, response);
    }


    @Override
    public void destroy() {
        System.out.println("Servlet is destroying");
        HibernateUtil.closeSessionFactory();
        System.out.println("Hibernate session factory has been closed");
    }
}
