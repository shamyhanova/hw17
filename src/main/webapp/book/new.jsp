<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <%@include file="../static/bootstrap.txt" %>
</head>
<body>


<div class="container">

    <h1>Добавить новую книгу</h1>

    <div class="row">
        <form action="../book/save"
              method="post"
        >
            <div class="mb-3">
                <label for="name" class="form-label">Имя</label>
                <input  required type="text" class="form-control" id="name" name="name">
            </div>

            <div class="mb-3">
                <label for="author" class="form-label">Автор</label>
                <input required type="text" class="form-control" id="author" name="author">
            </div>
            <div class="mb-4">
                <label for="izdanie" class="form-label">Издание</label>
                <input required type="text" class="form-control" id="izdanie" name="izdanie">
            </div>
            <div class="mb-5">
                <label for="releaseDate" class="form-label">Дата Релиза</label>
                <input required type="date" class="form-control" id="releaseDate" name="releaseDate">
            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>

    </div>
</div>

<a href="../book/save">Список</a>
</body>
</html>
