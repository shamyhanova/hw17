<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <%@include file="../static/bootstrap.txt" %>

    <title>List Books</title>

</head>
<body>
<div class="container">
    <div class="row">

        <h1>Список книг</h1>

        <table class="table">
            <tr>
                <td>ID</td>
                <td>Название</td>
                <td>Автор</td>
                <td>Издание</td>
                <td>Год выпуска</td>
            </tr>

            <c:forEach items="${book}" var="item">
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.author}</td>
                    <td>${item.izdanie}</td>
                    <td>${item.releaseDate}</td>
                </tr>
            </c:forEach>

        </table>


        <a href="new.jsp">Добавить еще</a>

    </div>
</div>

</body>
</html>
